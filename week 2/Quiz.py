# NUM_ROWS = 25
# NUM_COLS = 25

# # construct a matrix
# my_matrix = []
# for row in range(NUM_ROWS):
#     new_row = []
#     for col in range(NUM_COLS):
#         new_row.append(row * col)
#     my_matrix.append(new_row)
 
# # print the matrix
# for row in my_matrix:
#     print(row)

# print()

# def trace(matrix):
#     trace_num = 0
#     for row in range(len(matrix)):
#         trace_num += matrix[row][row]
#     return trace_num

# print(trace(my_matrix))

NUM_ROWS = 5
NUM_COLS = 9

# construct a matrix
my_matrix = {}
for row in range(NUM_ROWS):
    row_dict = {}
    for col in range(NUM_COLS):
        row_dict[col] = row * col
    my_matrix[row] = row_dict
    
print(my_matrix)
 
# print the matrix
for row in range(NUM_ROWS):
    for col in range(NUM_COLS):
        print(my_matrix[row][col], end = " ")
    print()

print()